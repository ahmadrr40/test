<?php

namespace App\Enum;

enum RoleEnum:string
{
    case admin = "admin";
    case driver ="driver";

    public static function asArray(): array
    {
        return array_map(fn($x) => $x->value, self::cases());
    }
}
