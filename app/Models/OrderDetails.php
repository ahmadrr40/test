<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OrderDetails extends Model
{
    use HasFactory;

    protected $fillable = [
        'from_time',
        'to_time',
        'date',
        'order_id'
    ];

    protected $casts = [
        'from_time:datetime:Y-m-d H:i:s',
        'to_time:datetime:Y-m-d H:i:s',
    ];

    public function order():BelongsTo
    {
        return $this->belongsTo(Order::class);
    }
}
