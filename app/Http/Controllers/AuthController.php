<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (!auth()->attempt(['username' => $request->username, 'password' => $request->password])) {
            return self::getJsonResponse('success', ['account'=>[0=>'main.invalid_email_or_password']], false, 422);
        }
        return self::getJsonResponse('success');
    }
}
