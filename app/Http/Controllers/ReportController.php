<?php

namespace App\Http\Controllers;

use App\Exports\ReportExport;
use App\Http\Requests\ReportRequest;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function index()
    {
        return view('reports.index',['drivers'=>User::getDrivers()]);
    }

    public function getReport(ReportRequest $request)
    {
        $start_date = Carbon::make($request->from_date.' '.$request->from_time)->toDateTimeString();
        $end_date = Carbon::make($request->to_date.' '.$request->to_time)->toDateTimeString();
        $reports = Order::with(['driver','orderDetails'])
        ->where('user_id',$request->driver)
        ->whereHas('orderDetails',function($query) use($start_date,$end_date){
           return  $query->where('from_time','>=',$start_date)->where('to_time','<=',$end_date);
        })->get();
        return self::getJsonResponse('success',['reports'=>$reports]);
    }

    public function export(ReportRequest $request,Excel $excel)
    {
        $start_date = Carbon::make($request->from_date . ' ' . $request->from_time)->toDateTimeString();
        $end_date = Carbon::make($request->to_date . ' ' . $request->to_time)->toDateTimeString();
        $reports = Order::with(['driver', 'orderDetails'])
            ->where('user_id', $request->driver)
            ->whereHas('orderDetails', function ($query) use ($start_date, $end_date) {
                return $query->where('from_time', '>=', $start_date)->where('to_time', '<=', $end_date);
            })->get();
        Excel::store(new ReportExport($reports), 'public/reports.xlsx');
        return response()->download(public_path('reports.xlsx'));
    }
}
