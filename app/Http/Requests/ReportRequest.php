<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'from_date' => ['required','date','date_format:Y-m-d'],
            'to_date' => ['required','date','date_format:Y-m-d','after_or_equal:'.$this->from_date],
            'from_time' => ['required','date_format:H:i'],
            'to_time' => ['required','date_format:H:i',Rule::when(($this->from_date == $this->to_date),['after:'.$this->from_time])],
            'driver' => ['required','integer','exists:users,id']
        ];
    }
}
