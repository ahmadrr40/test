<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('', function () {
    if(auth()->check())
        return view('index');
    return view('login');
})->name('login');

Route::post('/login',[AuthController::class,'login']);

Route::group(['prefix'=>'admin/','middleware' => 'auth'],function() {

    Route::get('/signout', function () {
        auth()->logout();
        return redirect('/');
    });
    Route::group(['prefix'=>'reports/','middleware' => 'role:'.\App\Enum\RoleEnum::admin->value],function (){
       Route::get('',[\App\Http\Controllers\ReportController::class,'index']);
        Route::post('/filter',[\App\Http\Controllers\ReportController::class,'getReport']);
        Route::post('/export',[\App\Http\Controllers\ReportController::class,'export']);
    });
});
