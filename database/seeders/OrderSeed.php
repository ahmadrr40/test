<?php

namespace Database\Seeders;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrderSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $order_1 = Order::create([
            'distance' => 100,
            'user_id' => 2
        ]);
        $order_1->orderDetails()->createMany([
           [
               'from_time' => Carbon::now()->subDays(10),
               'to_time' => Carbon::now()->addDays(10),
               'date' => date('Y-m-d'),
           ],[
                'from_time' => Carbon::now()->subDays(20),
                'to_time' => Carbon::now()->addDays(20),
                'date' => date('Y-m-d'),
            ]
        ]);

        $order_2 = Order::create([
            'distance' => 100,
            'user_id' => 2
        ]);

        $order_2->orderDetails()->createMany([
            [
                'from_time' => Carbon::now()->subDays(20),
                'to_time' => Carbon::now()->addDays(20),
                'date' => date('Y-m-d'),
            ],[
                'from_time' => Carbon::now()->subDays(30),
                'to_time' => Carbon::now()->addDays(30),
                'date' => date('Y-m-d'),
            ]
        ]);
    }
}
