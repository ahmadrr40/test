<?php

namespace Database\Seeders;

use App\Enum\RoleEnum;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = User::create([
            'username' => 'admin',
            'password' => 'admin@123'
        ]);
        $admin->assignRole(RoleEnum::admin->value);
        ##################################################
        $driver = User::create([
            'username' => 'driver',
            'password' => 'driver@123'
        ]);
        $driver->assignRole(RoleEnum::driver->value);
    }
}
