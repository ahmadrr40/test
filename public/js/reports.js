import {BaseAjaxClass} from './BaseAjaxClass.js';
import {enums} from './enums.js';
$(function(){
    var _url =  $("#base_url").val()+"/admin/reports",
        _frm = $('#frm');
        // _dateTime =  $('input[name="datetimes"]');
    // $('input[name="datetimes"]').daterangepicker({
    //     timePicker: true,
    //     timePicker24Hour:true,
    //     startDate: moment().startOf('hour'),
    //     endDate: moment().startOf('hour').add(32, 'hour'),
    //     locale: {
    //         format: 'YYYY-MM-DD HH:mm'
    //     }
    // });
    $('.date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });

    $('#from_timepicker').timepicker({
        uiLibrary: 'bootstrap4'
    });

    $('#to_timepicker').timepicker({
        uiLibrary: 'bootstrap4'
    });


    $(document).on("click","#btn_get",function() {
        BaseAjaxClass.clearErrorMsg();
        BaseAjaxClass.sendAjax(_url+"/filter",_frm.serialize(),enums.my_method.post);
        if(BaseAjaxClass.status == 200)
        {
            $('#report_table').find('tr').not(':first').remove();
            if(BaseAjaxClass.return_data.hasOwnProperty('reports'))
            {
                var reports = BaseAjaxClass.return_data.reports;
                jQuery.each(reports, $.proxy(function(index, item) {
                    $('#tbody').prepend(
                        `<tr class="text-center">
                          <td>${item.driver.username}</td>
                          <td>${item.id}</td>
                          <td>${item.distance}</td>
                       </tr>`
                    );
                }, this));
            }
        }
    });

    $(document).on("click","#btn_export",function() {
        BaseAjaxClass.clearErrorMsg();
        BaseAjaxClass.sendAjaxWithDownload(_url+"/export",_frm.serialize(),enums.my_method.post);
        if(BaseAjaxClass.status == 200)
            window.open($("#base_url").val() + "/storage/reports.xlsx", '_blank');
    });
});
