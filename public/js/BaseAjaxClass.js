import {enums} from './enums.js';
export class BaseAjaxClass {
    static return_data;
    static status;
    static  sendAjax(url,data,method)
    {
        if(method === enums.my_method.post)
            this.clearErrorMsg();
        $.ajax({
            url :  url,
            type: method,
            dataType:"json",
            data :data,
            async:false,
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response)
            {
                BaseAjaxClass.status = 200;
                BaseAjaxClass.return_data = response.data;
            }, error: function(e) {
                var alertContent = '';
                try {
                    var error = JSON.parse(e.responseText);
                    if(e.status == 422)
                    {
                        alertContent ='<ul>';
                        for (var key in error.data)
                            alertContent += '<li>'+error.data[key][0]+'</li>';
                        BaseAjaxClass.showErrorMsg(alertContent);
                    }
                    else
                    {
                        alertContent =  '<ul><li>'+error.message+'</li></ul>';
                        toastr.error(alertContent);
                    }

                } catch (err) {
                    toastr.error(err.message);
                }
            }
        });
    }

    static  sendAjaxWithDownload(url,data,method)
    {
        $.ajax({
            url :  url,
            type: method,
            data :data,
            headers:
            {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response)
            {
                BaseAjaxClass.status = 200;
                BaseAjaxClass.return_data = response.data;
            }, error: function(e) {
                var alertContent = '';
                try {
                    var error = JSON.parse(e.responseText);
                    if(e.status == 422)
                    {
                        alertContent ='<ul>';
                        for (var key in error.data)
                            alertContent += '<li>'+error.data[key][0]+'</li>';
                        BaseAjaxClass.showErrorMsg(alertContent);
                    }
                    else
                    {
                        alertContent =  '<ul><li>'+error.message+'</li></ul>';
                        toastr.error(alertContent);
                    }

                } catch (err) {
                    toastr.error(err.message);
                }
            }
        });
    }

    static showErrorMsg(msg)
    {
        $("#msg_alert").find('p').append(msg);
        $("#msg_alert").css("display","block");
    }

    static clearErrorMsg()
    {
        $("#msg_alert").find('p').text('');
        $("#msg_alert").css("display","none");
    }
}
