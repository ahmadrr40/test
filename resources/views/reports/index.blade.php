@extends('layouts.master')
@section("content")
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-olive">
                        <div class="d-flex justify-content-center align-items-center" id="overlay">
                            <i class="fas fa-2x"></i>
                        </div>
                        <div class="card-header">
                            <div class="row">
                                <div class="card-title col-md-12">
                                    <div class="form-group">
                                        <label for="statusSelect">{{__('main.please_fill_inputs')}}:</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="card-body">
                            <div class="alert alert-danger alert-dismissible" id="msg_alert" style="display: none">
                                <p></p>
                            </div>
                            <form id="frm">
                            <div class="form-group row">
                                <div class="col-6">
                                    <label for="from_date">from_date:</label>
                                    <input type="text" class="date"  id="from_date" name="from_date" style="width: 600px;!important;" />
                                </div>
                                <div class="col-6">
                                    <label for="to_date">to_date:</label>
                                    <input type="text" class="date"  id="to_date" name="to_date" style="width: 600px;!important;" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-6">
                                    <label for="from_time">from_time:</label>
                                    <input id="from_timepicker" name="from_time"/>
                                </div>
                                <div class="col-6">
                                    <label for="to_time">to_time:</label>
                                    <input id="to_timepicker" name="to_time"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="">Driver:</label>
                                    <select class="form-control select2" name="driver" id ="driver">
                                        <option selected value="-1">Please Select Driver</option>
                                        @foreach($drivers as $driver)
                                            <option value="{{$driver->id}}">{{$driver->username}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        </form>
                        <div class="card-footer">
                            <div class="row">
                                <div class=" col-md-12">
                                    <button class="card-title btn btn-success col-md-12" id="btn_get"><i class="fas fa-search"></i></button>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button class="card-title btn btn-primary col-md-12" id="btn_export"><i class="fas fa-file-excel"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-olive">
                        <div class="d-flex justify-content-center align-items-center" id="overlay">
                            <i class="fas fa-2x"></i>
                        </div>
                        <div class="card-header">
                        </div>
                        <div class="card-body" id="card-body">
                            <table id="report_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>order number</th>
                                    <th>distance</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">
                                </tbody>
                            </table>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script src={{asset("js/reports.js")}} type="module"></script>
@endsection


