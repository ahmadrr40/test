@extends('layouts.master')
@section("content")
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-outline card-olive">
                        <div class="d-flex justify-content-center align-items-center" id="overlay">
                            <i class="fas fa-2x"></i>
                        </div>
                        <div class="card-header">
                        </div>
                        <div class="card-body" id="card-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>order number</th>
                                    <th>distance</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($reports as $report)
                                    <tr>
                                        <td>{{$report->driver->username}}</td>
                                        <td>{{$report->id}}</td>
                                        <td>{{$report->distance}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <br>
                            <div class="show-paginate">
                                Showing {{($reports->currentpage()-1)*$reports->perpage()+1}}
                                to {{$reports->currentpage()*$reports->perpage()}}
                                of {{$reports->total()}} entries
                            </div>
                            {{ $reports->links("pagination::bootstrap-4") }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection


