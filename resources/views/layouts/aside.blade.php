
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="btn bg-purple btn-flat"  href="{{URL::TO('admin/signout')}}"><i class="fas fa-sign-out-alt">Signout</i></a>
        </li>
    </ul>
</nav>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <p class="brand-link" style="padding-left: 20px;">
        <span class="brand-text font-weight-light">
            <i class="fas fa-user"></i>
           {{auth()->user()->username}}
        </span>
    </p>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{URL::to('admin/reports')}}" class="nav-link {{(Request()->segment(2) == 'reports')?'active':''}}">
                        <i class="far fa-address-book nav-icon"></i>
                        <p>{{__('main.reports')}}</p>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
<div class="content-wrapper">

