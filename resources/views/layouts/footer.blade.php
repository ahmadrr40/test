</body>
<!-- jQuery -->
<script src={{asset("js/jquery.min.js")}}></script>
<script src={{asset("js/bootstrap.bundle.min.js")}}></script>
<script src={{asset("js/adminlte.min.js")}}></script>
<!-- dataTables -->
<script src={{asset("plugin/datatables/jquery.dataTables.min.js")}}></script>
<script src={{asset("plugin/datatables-bs4/js/dataTables.bootstrap4.min.js")}}></script>
<!-- Select2 -->
<script src={{asset("plugin/select2/js/select2.full.min.js")}}></script>
<!-- Toastr -->
<script src={{asset("plugin/toastr/toastr.min.js")}}></script>
<!--datetime-->
<script type="text/javascript" src="{{asset('plugin/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugin/daterangepicker.min.js')}}"></script>
<script src="https://unpkg.com/gijgo@1.9.14/js/gijgo.min.js" type="text/javascript"></script>
<script src={{asset("js/main.js")}}></script>
@yield('script')
</html>
