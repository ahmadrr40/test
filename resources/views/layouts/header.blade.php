<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Test</title>
    <input type="text" value="{{url('/')}}" style="display:none" id="base_url">
    <link rel="stylesheet" href={{asset("css/adminlte.min.css")}}>
    <link rel="stylesheet" href={{asset("plugin/fontawesome-free/css/all.min.css")}}>
    <!-- datatables -->
    <link rel="stylesheet" href={{asset("plugin/datatables-bs4/css/dataTables.bootstrap4.min.css")}}>
    <!-- Select2 -->
    <link rel="stylesheet" href={{asset("plugin/select2/css/select2.min.css")}}>
    <link rel="stylesheet" href={{asset("plugin/select2-bootstrap4-theme/select2-bootstrap4.min.css")}}>
    <!-- Toastr -->
    <link rel="stylesheet" href={{asset("plugin/toastr/toastr.min.css")}}>
    <!--datetime-->
    <link rel="stylesheet" type="text/css" href="{{asset('plugin/daterangepicker.css')}}" />
    <link href="https://unpkg.com/gijgo@1.9.14/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="hold-transition sidebar-mini text-sm">
<div class="wrapper">
