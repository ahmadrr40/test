<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Nano Test</title>
    <!-- base url-->
    <input type="text" value="{{url('/')}}" style="display:none" id="base_url">
    <!-- Theme style -->
    <link rel="stylesheet" href={{asset("css/adminlte.min.css")}}>
    <!-- Toastr -->
    <link rel="stylesheet" href={{asset("plugin/toastr/toastr.min.css")}}>
    <!--csrf-->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="hold-transition" class="login-page">
<div class="h-100 d-flex align-items-center justify-content-center vh-100">
    <div class="login-box">
        <div class="card">
            <div class="d-flex justify-content-center align-items-center" id="overlay">
                <i class="fas fa-2x"></i>
            </div>
            <div class="card-body login-card-body ">
                <p class="login-box-msg">{{__('main.please_login')}}</p>
                <div class="alert alert-danger alert-dismissible" id="msg_alert" style="display:none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5></h5>
                    <p></p>
                </div>
                    <div class="input-group mb-6">
                        <input  type="text" class="form-control" id="username" name="username" placeholder="{{__('main.username')}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-user"></span>
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control"id="password" name="password" placeholder="{{__('main.password')}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button  class="btn btn-primary btn-block" id="btn_signin">
                                <i class="fas fa-login"></i>{{__('main.signin')}}
                            </button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script src={{asset("js/jquery.min.js")}}></script>
<script src={{asset("js/bootstrap.bundle.min.js")}}></script>
<script src={{asset("js/adminlte.min.js")}}></script>
<!-- Toastr -->
<script src={{asset("plugin/toastr/toastr.min.js")}}></script>
<!-- login -->
<script src={{asset("js/login.js")}} type="module"></script>
</body>
</html>
